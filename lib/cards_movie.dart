import 'package:disney_movies/ranking_stars.dart';
import 'package:flutter/material.dart';

class MoviesCard extends StatefulWidget {
  final String nameMovie;
  final String photo;
  final int stars;
  const MoviesCard(this.nameMovie, this.photo, this.stars, {Key? key})
      : super(key: key);

  @override
  State<MoviesCard> createState() => _MoviesCardState();
}

class _MoviesCardState extends State<MoviesCard> {
  bool watched = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        child: Stack(
          children: [
            Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    gradient: LinearGradient(
                      colors: [
                        Color.fromARGB(255, 24, 27, 163).withOpacity(0.3),
                        Colors.purple,
                      ],
                    ),
                    // color: Color.fromARGB(255, 192, 171, 228),
                  ),
                  height: 200,
                  child: Row(
                    children: [
                      SizedBox(
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              color: Colors.black26),
                          height: 200,
                          width: 150,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(6),
                            child: Image.network(
                              widget.photo,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            alignment: Alignment.topRight,
                            padding: EdgeInsets.only(left: 8),
                            width: 100,
                            child: Text(
                              widget.nameMovie,
                              style: const TextStyle(
                                fontSize: 18,
                                overflow: TextOverflow.clip,
                              ),
                            ),
                          ),
                          Ranking(
                            rankingStars: widget.stars,
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          ElevatedButton(
                            onPressed: () {
                              setState(() {
                                watched = !watched;
                              });
                            },
                            child: const Icon(Icons.favorite),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Text(
                  watched ? 'Assistido' : 'Não Assistido',
                  style: const TextStyle(color: Colors.black),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
