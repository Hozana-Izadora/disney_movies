import 'package:disney_movies/ranking_stars.dart';
import 'package:flutter/material.dart';

import 'cards_movie.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Disney Movies',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
            seedColor: const Color.fromARGB(255, 151, 95, 247)),
        useMaterial3: true,
      ),
      home: Scaffold(
        backgroundColor: Color.fromARGB(255, 80, 76, 172),
        appBar: AppBar(
          leading: Container(),
          title: const Text(
            'Filmes',
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Color.fromARGB(255, 59, 61, 119),
        ),
        body: ListView(
          children: [
            MoviesCard(
                'A Bela e a Fera',
                'https://stories.adorocinema.com/a-bela-e-a-fera-tem-conexao-com-outra-animacao-da-disney/assets/1.jpeg',
                5),
            MoviesCard(
                'A Branca de Neve e os 7 anões',
                'https://p2.trrsf.com/image/fget/cf/1200/1200/middle/images.terra.com/2022/01/29/404877765-disneysnow-white-and-the-seven-dwarves.jpg',
                3),
            MoviesCard(
                'A pequena Sereia',
                'https://jpimg.com.br/uploads/2021/09/cropped-disney-the-little-mermaid.jpg',
                4),
          ],
        ),
      ),
    );
  }
}
