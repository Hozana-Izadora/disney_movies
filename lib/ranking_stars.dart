import 'package:flutter/material.dart';

class Ranking extends StatelessWidget {
  final int rankingStars;

  const Ranking({
    required this.rankingStars,
    super.key,
  });
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          Icons.star,
          color: rankingStars >= 1 ? Colors.amber : Colors.amber.shade100,
        ),
        Icon(
          Icons.star,
          color: rankingStars >= 2 ? Colors.amber : Colors.amber.shade100,
        ),
        Icon(
          Icons.star,
          color: rankingStars >= 3 ? Colors.amber : Colors.amber.shade100,
        ),
        Icon(
          Icons.star,
          color: rankingStars >= 4 ? Colors.amber : Colors.amber.shade100,
        ),
        Icon(
          Icons.star,
          color: rankingStars >= 5 ? Colors.amber : Colors.amber.shade100,
        ),
      ],
    );
  }
}
